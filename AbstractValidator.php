<?php

/**
 * Class AbstractValidator
 *
 *
 * @author Исаков Владислав
 */
abstract class AbstractValidator {
	public string $value;

	private array $_errors = [];

	abstract public function validate($paramName);

	/**
	 * @param       $value
	 *
	 * @author Isakov Vlad <isakov.vi@dns-shop.ru>
	 *
	 * AbstractValidator constructor.
	 */
	public function __construct($value) {
		$this->value = $value;
	}

	public function addError($param, $msg) {
		$this->_errors[] = [
			$param => $msg
		];
	}

	public function getError(): array {
		return $this->_errors;
	}
}