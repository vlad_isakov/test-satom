<?php
/**
 * @author Исаков Владислав
 */
include_once "StringValidator.php";
include_once "EmailValidator.php";

abstract class AbstractForm {
	private array $_errors = [];

	/**
	 * Список валидаторов для полей формы в формате:
	 *
	 * return [
	 *    'name'  => [StringValidator::class, ['min' => 5, 'max' => 255]],
	 *    'email' => [EmailValidator::class]
	 * ];
	 *
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	abstract public function rules(): array;


	/**
	 * Валидация формы
	 *
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public function validate(): bool {
		foreach ($this->rules() as $param => $validateRules) {
			/** @var \AbstractValidator $validator */
			$validator = new $validateRules[0]($this->$param);

			if (false === $validator instanceof AbstractValidator) {
				continue;
			}

			if (array_key_exists(1, $validateRules)) {
				foreach ($validateRules[1] as $validateParam => $validateParamValue) {
					$validator->$validateParam = $validateParamValue;
				}
			}

			if (false === $validator->validate($param)) {
				foreach ($validator->getError() as $error) {
					$this->addError($error);
				}

				return false;
			}
		}

		return true;
	}

	/**
	 * @param array $error
	 *
	 * @author Исаков Владислав
	 */
	public function addError(array $error) {
		$this->_errors[] = $error;
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function getErrors(): array {
		return $this->_errors;
	}
}