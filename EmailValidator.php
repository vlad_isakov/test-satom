<?php
include_once "AbstractValidator.php";

class EmailValidator extends AbstractValidator {
	public function validate($paramName) {
		if (false === filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
			$this->addError($paramName, 'Указан некоррректный email-адрес');

			return false;
		}

		return true;
	}
}