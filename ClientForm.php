<?php
include_once "AbstractForm.php";

/**
 * Class ClientForm
 *
 *
 * @author Исаков Владислав
 */
class ClientForm extends AbstractForm {

	public string $name;

	public string $email;

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function rules(): array {
		return [
			'name'  => [StringValidator::class, ['min' => 5, 'max' => 255]],
			'email' => [EmailValidator::class]
		];
	}
}