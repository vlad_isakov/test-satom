<?php
include_once  "AbstractValidator.php";

class StringValidator extends AbstractValidator {
	public int $min = 10;

	public int $max = 255;

	public function validate($paramName) {
		if (false === is_string($this->value)) {
			$this->addError($paramName, 'Не является строкой');

			return false;
		}

		if (strlen($this->value) < $this->min) {
			$this->addError($paramName, 'Минимальное кол-во символов:' . $this->min);

			return false;
		}

		if (strlen($this->value) > $this->max) {
			$this->addError($paramName, 'Максимальное кол-во символов:' . $this->min);

			return false;
		}

		return true;
	}
}